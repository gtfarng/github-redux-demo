import React, {Component}  from 'react';
// import axios from "axios"
import {getApi} from '../action'
import {connect} from 'react-redux'

class Github extends Component {

    componentDidMount(){
        console.log('props', this.props)
        this.props.getApi()
    }
    
    renderUser = () => {
        // console.log(data)
        console.log('prop: ', this.props.data)
        if(this.props.data){
           return  <div>
            <img src={this.props.data.avatar_url} alt="avatar" width="200px"/><br/><br/>
          <strong> Username </strong>: {this.props.data.login}<br/> 
         <strong>  NAME </strong>: {this.props.data.name}<br/> 
          <strong> ID </strong>: {this.props.data.id}<br/> 
          <strong> URL </strong>: {this.props.data.blog}<br/> 
           </div>
        }
    }
    render() {
      return (
          <div align="center">
              <h2> RENDER USER</h2>  
              <ul>
                  {this.renderUser()}
              </ul>
          </div>
      )
    }
 }

 const mapStateToProps = ({data}) => {
     return {data}
 }

 const mapDispatchToProps = (dispatch) => {
     return {
         getApi: () => dispatch(getApi())
     }
 }
 
 export default connect (mapStateToProps,mapDispatchToProps)(Github)